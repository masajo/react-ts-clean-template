import { Email, UniqueID } from "../shared/shared.types";
import { Ingredient } from "./ingredients";

export type UserName = string;

export type User = {
    id: UniqueID;
    email: Email;
    name: UserName;
    favIngredients: Ingredient[];
    allergies: Ingredient[];
}

/**
 * Function to know if an ingredient is fav for a user
 * @param user 
 * @param ingredient 
 * @returns If the ingredient is fav for the user
 */
export function hasPreference(user: User, ingredient: Ingredient): boolean {
    return user.favIngredients.includes(ingredient);
}

/**
 * Function to know if a user is allergic to an ingredient
 * @param user
 * @param ingredient 
 * @returns If  the user is allergic to the ingredient or not
 */
export function hasAllergy(user:User, ingredient: Ingredient): boolean {
    return user.allergies.includes(ingredient);
}
