import { DateTimeString, Price, UniqueID } from "../shared/shared.types";
import { Cart } from "./cart";
import { totalPrice } from "./product";
import { User } from "./user";

export type OrderStatus = 'new' | "delivery" | "completed";

export type Order = {
    user: UniqueID;
    cart: Cart;
    createdAt: DateTimeString;
    status: OrderStatus;
    total: Price
}


export function createOrder(user: User, cart: Cart): Order{
    return {
        user: user.id,
        cart: cart,
        status: "new",
        createdAt: Date.now().toLocaleString(),
        total: totalPrice(cart.products)
    }
}