import { Product } from "./product";

export type Cart = {
    products: Product[]
}

export function addProduct(cart: Cart, product: Product): Cart {
    return {
        ...cart,
        products: [
            ...cart.products,
            product
        ]
    }
}