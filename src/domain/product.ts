import { Price, UniqueID } from "../shared/shared.types";
import { Ingredient } from "./ingredients";

export type Title = string;

// Product Type
export type Product = {
    id: UniqueID;
    title: Title;
    price: Price;
    ingredients: Ingredient[];
}

/**
 * Function to obtain total price of products
 * @param products 
 * @returns Te total price of a list o products
 */
export function totalPrice(products: Product[]): Price {
    // We sum all prices of the list of products
    return products.reduce((total, {price}) => total + price, 0);
}


export const ingredients: Record<Ingredient, string> = {
    salt: 'Salt',
    sugar: 'Sugar',
    chocolate: 'Chocolate'
}

