import { Cart } from "../domain/cart";
import { Order } from "../domain/order";
import { User, UserName } from "../domain/user";
import { Email, Message, Price } from "../shared/shared.types";


// Interfaces to communicate our app with external resources

// User
export interface UserStorageService {
    user?: User;
    updateUser(user: User): void
}

export interface AuthenticationService {
    auth(name: UserName, email: Email): Promise<User>
}

// Cart
export interface CartStorageService {
    cart?: Cart;
    updateCart(cart: Cart): void;
    clearCart(): void;
}

// Order
export interface OrdersStorageService {
    orders: Order[],
    updateOrders(orders: Order[]): void
}

// Notifications & Payments

export interface PaymentService {
    pay(amount: Price): Promise<boolean>
}

export interface NotificationService {
    notify(message: Message): void
}
