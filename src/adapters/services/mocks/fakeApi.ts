/**
 * Function to fake an API Call & obtain response
 * @param response 
 * @returns Promise with generic Reponse
 */
export function fakeApi<TResponse>(response: TResponse): Promise<TResponse>{
    return new Promise((res) => {
            setTimeout(() => res(response), 500);
        })
}