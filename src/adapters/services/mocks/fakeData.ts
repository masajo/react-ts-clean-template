/**
 * Faked List of Products
 */
import { Product } from '../../../domain/product';

export const storeProducts: Product[] = [
    {
        id: '1',
        title: 'Producto 1',
        price: 100,
        ingredients: ['salt', 'chocolate' ]
    },
    {
        id: '2',
        title: 'Producto 2',
        price: 50,
        ingredients: ['chocolate' ]
    },
    {
        id: '3',
        title: 'Producto 3',
        price: 180,
        ingredients: ['sugar', 'salt', 'chocolate' ]
    },
]