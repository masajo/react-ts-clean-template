import React, {useState, useContext} from "react";
import { storeProducts } from './mocks/fakeData';

/**
 * We define the store context of the app
 */
const StoreContext = React.createContext<any>({});
export const useStore = () => useContext(StoreContext);

export const Provider: React.FC = ({children}) => {

    // We define the states
    const [user, setUser] = useState();
    const [cart, setCart] = useState();
    const [orders, setOrders] = useState();

    const providedValue = {
        user, 
        orders,
        cart,
        updateUser: setUser,
        updateOrders: setOrders,
        updateCart: setCart,
        // Clear Cart
        clearCart: () => {
            // TODO: Set empty Cart products
            console.warn('Not implemented Method: Empty Cart')
        }
    }


    return (
        <StoreContext.Provider value={providedValue}>
            { children }
        </StoreContext.Provider>
    )


}
