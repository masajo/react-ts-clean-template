import { PaymentService } from '../../application/ports';
import { Price } from '../../shared/shared.types';
import { fakeApi } from './mocks/fakeApi';


export function usePayment(): PaymentService {
    return {
        pay(amount: Price) {
            return fakeApi(true);
        }
    }
}