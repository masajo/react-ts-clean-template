import { NotificationService } from '../../application/ports';
import { Message } from '../../shared/shared.types';

export function useNotificate(): NotificationService {
    return {
        notify: (message: Message) => {
            window.alert(`Notification: ${message}`);
        }
    }
}