import { 
    CartStorageService, 
    OrdersStorageService, 
    UserStorageService } from "../../application/ports";

import { useStore } from './store';

// User Store Adapter
export function useUserStorage(): UserStorageService {
    return useStore();
}

// Orders Store Adapter
export function useOrdersStorage(): OrdersStorageService {
    return useStore();
}

// Cart Store Adapter
export function useCartStorage(): CartStorageService {
    return useStore();
}