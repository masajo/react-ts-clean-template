import { UserName } from "../../domain/user"; 

import { AuthenticationService } from "../../application/ports";
import { fakeApi } from "./mocks/fakeApi";
import { Email } from "../../shared/shared.types";


export function useAuth(): AuthenticationService {
    return {
        auth(name: UserName, email: Email){
            return fakeApi({
                id: 'User ID',
                name: name,
                email: email,
                allergies: [ 'salt', 'chocolate' ],
                favIngredients: [ 'sugar' ]
            })
        }
    }
}
