// Shared types between layers
export type Email = string;
export type Price = number;
export type DateTimeString = string;
export type UniqueID = string;
export type Message = string;
// etc.