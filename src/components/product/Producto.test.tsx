import React from 'react';

import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ProductoComponent, { ProductoProps } from './Producto';


const PRODUCTO_DEFAULT_PROPS: ProductoProps = {
    producto: {nombre: 'Ejemplo', cantidad: 0, precio: 500, imagen: 'ejemplo.png'},
    eliminar: jest.fn(),
    anadir: jest.fn(),
}

const renderComponent = (props: Partial<ProductoProps> = {}) => {
    return {
        ...render(<ProductoComponent {...PRODUCTO_DEFAULT_PROPS} {...props} />)
    }
}


describe.skip('El producto se muestra correctamente', () => {

    test('El producto muestra su nombre', () => {
        renderComponent();
        expect(screen.getByText(PRODUCTO_DEFAULT_PROPS.producto.nombre)).toBeInTheDocument();
    });
    
    // TODO: Comprobar resto de propiedades del Producto
    test.todo('El producto muestra su precio');
    test.todo('El producto muestra su imagen');
    test.todo('El producto muestra su cantidad');

});

// test.each`
//     descuento | precio | promocion
//     ${10}     | ${100} | ${false}
//     ${50}     | ${90}  | ${true}
//     ${20}     | ${100} | ${false}
//     ${50}     | ${500} | ${true}
//     ${20}     | ${300} | ${true}
// `('El producto debería mostrar el precio descontado'), ({descuento, precio, promocion}) => {
//     renderComponent({
//         producto: { ...PRODUCTO_DEFAULT_PROPS.producto, precio, promocion }
//     })

//     if(promocion){
//         expect(screen.getByText('Promoción habilitada')).toBeInTheDocument();
//     }else {
//         expect(screen.getByText('Promoción habilitada')).toBeFalsy();
//     }

// }


// Prueba Rerender

test.skip('Habilita el botón de reducir cantidad si cantidad es mayor a 0', () => {
    
    const { rerender } = renderComponent();

    // Buscamos el butón para reducir la cantidad de producto a comprar
    expect(screen.getByRole('button', { name: /\-/i })).toBeDisabled();

    // Creamos unas nuevas props para el componente Producto
    const nuevasProps: ProductoProps = {
        ...PRODUCTO_DEFAULT_PROPS,
        producto: {
            ...PRODUCTO_DEFAULT_PROPS.producto,
            cantidad: 3
        }
    }

    // Renderizamos de nuevo el componente con nuevas props
    rerender(<ProductoComponent {...nuevasProps} />);

    // Buscamos el butón para reducir la cantidad de producto a comprar
    expect(screen.getByRole('button', { name: /\-/i })).not.toBeDisabled();

})

test.skip('Añadir cantidad de producto despacha un evento', () => {
    
    renderComponent();

    userEvent.click(screen.getByRole('button',  { name: /\+/i }))

    // Verificamos que se haya llamado al método anadir de las Props del componente
    expect(PRODUCTO_DEFAULT_PROPS.anadir).toBeCalled();
    expect(PRODUCTO_DEFAULT_PROPS.anadir).toBeCalledTimes(1);
    expect(PRODUCTO_DEFAULT_PROPS.anadir).toBeCalledWith(PRODUCTO_DEFAULT_PROPS.producto.nombre);
})

