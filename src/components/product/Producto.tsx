import { Producto } from "../../types/producto.type";

export interface ProductoProps {

    producto: Producto;
    eliminar: () => void;
    anadir: () => void;
}

const ProductoComponent = (props: ProductoProps) => {

    return <></>
}

export default ProductoComponent;
