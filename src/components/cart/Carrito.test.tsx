import React from 'react';

// Testing Library de React
import { screen, render  } from '@testing-library/react';
// Testing de eventos de usuario
import userEvent from '@testing-library/user-event';

// Props del Carrito
import Carrito, { CarritoProps } from './Carrito';

const CARRITO_DEFAULT_PROPS: CarritoProps = {
    productos: [],
    promociones: [],
    eliminarProducto: jest.fn(),
    anadirProducto: jest.fn()
}

// Creamos una función reutilziable para renderizar el componente
const renderComponent = (props: Partial<CarritoProps> = {}) => {
    return {
        ...render(<Carrito {...CARRITO_DEFAULT_PROPS} {...props} />)
    }
}


xdescribe('Productos del Carrito', () => {

    test('El Carrito debe renderizar productos', () => {
        // Lo primero que hacemos es renderizar el componente
        renderComponent();
        // Verificamos que en la vista se renderizan los productos adecuadamente
        CARRITO_DEFAULT_PROPS.productos.forEach((producto) => {
            // Comprobamos que se renderice este producto
            expect(screen.getByText(producto)).toBeInTheDocument();
        })
    });
    
    
    describe.skip('Precios del Carrito', () => {
            test('El Carrito debería mostrar el precio correctamente', () => {
                renderComponent();
                // Establecemos un valor esperado
                const precioTotalEsperado = 100;
                // Búsqueda por expresión regular para encontrar en la pantalla el precioEsperado
                expect(screen.getByText(new RegExp(`${precioTotalEsperado}`, 'i'))).toBeInTheDocument();
            })
        
            test('El Carrito debería mostrar el precio de envío correctamente', () => {
                renderComponent();
                expect(screen.getByText(/50.50/i)).toBeInTheDocument();
            })

            test.skip('A partir de 200€, el envío debe ser gratis', () => {
                
                // renderComponent({
                //     productos: [
                //         ...CARRITO_DEFAULT_PROPS.productos,
                //         {
                //             nombre: 'Producto Caro',
                //             precio: 100
                //         }
                //     ]
                // });

                // Verificamos que aparece el envío como gratuito
                // expect(screen.getByText(/envío gratis/i)).toBeInTheDocument();

            })

            describe.skip('Códigos de Descuento', () => {

                test('El Carrito permite códigos de descuento válido', () => {
                    
                    renderComponent();
                    
                    const nombreCodigoDescuento = 'BLACKFRIDAY';
                    const descuento = 10;
    
                    // Verificamos que la información del código de descuento aparece en la vista
                    // Para ello hacemos que el usuario escribe en el campo de texto indicado el código de descuento
                    
                    // 1. Obtemos el campo donde escribir el nombre del código y lo escribimos con TYPE
                    userEvent.type(screen.getByRole('textbox'), nombreCodigoDescuento);
                    
                    // 2. Simulamos un CLICK en el botón para aplicar el descuento
                    // Lo buscamos por nombre
                    userEvent.click(screen.getByRole('button', { name: /aplicar descuento/i}));
    
                    // Al hacer click, verificamos que el descuento se ha aplicado
                    expect(screen.getByText(/descuento aplicado: /i)).toHaveTextContent(`${descuento}`);
    
                })


                test('El Carrito debe actualizar el precio total cuando se aplica un código válido', () => {
                    renderComponent();
                    
                    const nombreCodigoDescuento = 'BLACKFRIDAY';
                    const descuento = 10;
    
                    // Verificamos que la información del código de descuento aparece en la vista
                    // Para ello hacemos que el usuario escribe en el campo de texto indicado el código de descuento
                    
                    // 1. Obtemos el campo donde escribir el nombre del código y lo escribimos con TYPE
                    userEvent.type(screen.getByRole('textbox'), nombreCodigoDescuento);
                    
                    // 2. Simulamos un CLICK en el botón para aplicar el descuento
                    // Lo buscamos por nombre
                    userEvent.click(screen.getByRole('button', { name: /aplicar descuento/i}));

                    // Al hacer click, verificamos que el total del carrito se ha actualizado
                    expect(screen.getByText(/Total: /i)).toHaveTextContent(new RegExp(`${100 - descuento}`, 'i'));

                })
                
                
                test('El Carrito no permite códigos de descuento inválidos', () => {
                    renderComponent();

                    const nombreCodigoDescuento = 'INVALIDO';
                    
                    // 1. Obtemos el campo donde escribir el nombre del código y lo escribimos con TYPE
                    userEvent.type(screen.getByRole('textbox'), nombreCodigoDescuento);
                    
                    // 2. Simulamos un CLICK en el botón para aplicar el descuento
                    // Lo buscamos por nombre
                    userEvent.click(screen.getByRole('button', { name: /aplicar descuento/i}));
    
                    // Al hacer click, verificamos que el descuento se ha aplicado
                    expect(screen.getByRole('alert')).toMatchInlineSnapshot(
                        `
                        <span class="error" role="alert">
                            Código de descuento no válido
                        </span>
                        `
                    );

                })
            })
    })
    
})


