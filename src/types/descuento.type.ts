export interface DescuentoType {
    nombre: string;
    descuento: number;
}