export interface Producto {
    nombre: string;
    precio: number;
    cantidad: number;
    promocion?: boolean;
    imagen: string;
}