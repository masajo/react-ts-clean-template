// Testing examples with Cypress

describe('Example Cypress TO-DO App', () => {
    
    beforeEach(() => {
        // Visit the URL (navigate)
        cy.visit('https://example.cypress.io/todo');
    });

    it('Display two items', () => {
        
        // We obtain the todo list
        cy.get('.todo-list li').should('have.length', 2);

        // Check value in <li></li>
        cy.get('.todo-list li').first().should('have.text', 'Pay electric bill');
        cy.get('.todo-list li').last().should('have.text', 'Walk the dog');

    });

    xit('Login command example', () => {
        cy.login('admin@admin.com', 'nimda') // it will do all steps
    })

    context('User is logged in', () => {

        beforeEach(() => {
            cy.login('Admin', 'admin123')
        })

        // Al tests have the user logged in
        // ....
    })

});