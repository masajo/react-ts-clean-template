@Authentication
Feature: Autentication Feature

    Scenarios to prove login/logout features

    Scenario: Admin user Login Success
        Given The user is in Login Page
        When The user inserts "Admin" as username in login form
        And The user inserts "admin123" as password in login form
        And The user submits login form
        Then The welcome message "Welcome Admin" is shown in navbar

    Scenario: Nimda user Login Failure
        Given The user is in Login Page
        When The user inserts "Nimda" as username in login form
        And The user inserts "nimda123" as password in login form
        And The user submits login form
        Then The error message "Invalid Credentials" is shown in login form

    Scenario Outline: User list Login Success
        Given The user is in Login Page
        When The user inserts <username> as username in login form
        And The user inserts <password> as password in login form
        And The user submits login form
        Then The welcome message <message> is shown in navbar
        Examples:
            | username | password | message        |
            | Admin1   | admin123 | Welcome Admin1 |
            | Admin2   | admin123 | Welcome Admin2 |
            | Admin3   | admin123 | Welcome Admin3 |