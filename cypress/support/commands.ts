// File to write own Cypress Commands

// login Command implementation
Cypress.Commands.add('login', (email:string, password:string) => {
    cy.get('.emailInput').type(email);
    cy.get('.passwordInput').type(password);
    cy.get('button').click();
});