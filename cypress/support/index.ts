import './commands';

/**
 * Declare type definitions for commands
 */

declare global {
    namespace Cypress {
        interface Chainable {
            /**
             * Define custom Cypress commands to be implemented in commands.ts
             */
            login(email:string, password: string): void
        }
    }
}